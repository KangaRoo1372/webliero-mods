# Liero 1.33

This is the original, unchanged Liero. 

## How to use

To use the mod:
- download all its files from this folder
- open chat (`enter` by default)
- type `/loadmod` and select both `mod.json5` (this contains "logic" changes of the mod) and `sprites.wlsprt` (this contains gfx changes of the mod)
