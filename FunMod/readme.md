# Fun Mod by KangaRoo (a.k.a. Kangur)

This repository contains my own works regarding WEBLIERO - an online version (made by basro) of classic MS-DOS freeware game called LIERO made by Joosa Riekkinen and released in 1998. It contains my own mod called "Fun Mod". The mod was created for several weeks in total. During this time, new ideas for experimental and usually funny weapons were constantly developed. Many of them were tested with many players who not only helped with mod testing, weapon improvement and mod balancing, but also gave inspiration to create new weapons - to whom I would like to thank. The mod is based on Szarnywirk's promode rerevisited.

To use the mod, download the file "mod.json5" and "sprites.wlsprt" from this repo, then run webliero in your browser, create your own room*, open the chat window (enter by default), type /loadmod in the console and select the mod from the directory (where you downloaded the mod). The mod was made using standard gfx from promode mod, so that it works the best with sprites from promode. So, to load the full mod, you should choose both files together (json5 and wlsprt file) after typing "loadmod" in the chat window.

* you can also load the mod on the room which was not created by yourself, but you need to have admin rights granted there.

# Changelog

## 1.0

Initial commit.

## 1.07

New weapon added - Szarnywirk's Drone.

## 1.11

Changed colour of blood & rope + some changes into (also renamed into "coil gun").

## 1.12

Some railgun bugs fixed.

## 1.16

Fiery missile + blaster bugs fixed.

## 1.21

Added new weapon - ectoplasm gun.

## 1.22

Some changes to blaster + railgun + corpse slasher.

## 1.27

Some changes to ectoplasm gun + coil gun + drone.

## 1.31

Some weapons changed: drone, magnetic shield, blaster, railgun and doppelgaenger.

## 1.32

Some changes to ectoplasm gun and acid fan (also renamed to "miasma blower").

# 1.33 (hehe)

Make bonuses bounce + ninja rope colour change.

# 1.34

Some changes to: dragon flies (movement bug fixed + some dmg added), railgun (trailobj dirteffect change), ectoplasm gun (trailobj dirteffect + starframe change), magnetic shield (trailobjt dirteffect change) + remove drone.

# 1.35a

Some changes to toxic smoke and hot steam.

# 1.35b

Some changes to shockwave bomb and firecracker.

# 1.35c

Some changes to doppelgaenger and shockwave bomb.

# 1.36

Significant changes to ectoplasm gun.
